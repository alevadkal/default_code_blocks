/// \file
/// \brief      Main test
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#include <stdio.h>
#include "test.h"

int main()
{
    logFile = stderr;
    LOG_I("TestStarted");
    return UnitTest::RunAllTests();
}
