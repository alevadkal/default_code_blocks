/// \file
/// \brief      Memory wrapper tests
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#include "test.h"
#include <memory.h>
#include <stdint.h>

#include "memory_wrapper.h"


namespace
{
SUITE(memory_wrapper_test)
{
    struct BasePtr
    {
        const size_t length = 12;
        void* ptr=NULL;
        void* zero_array=NULL;
        void* non_zerro_array=NULL;
        BasePtr()
        {
            zero_array = calloc(length,1);
            if(zero_array == NULL)
            {
                TEST_LOG("calloc error");
                exit(1);
            }
            non_zerro_array = calloc(length,length);
            if(non_zerro_array == NULL)
            {
                TEST_LOG("malloc error");
                exit(1);
            }
            memset(non_zerro_array, 0x55, length*length);
        }
        ~BasePtr()
        {
            if(ptr != NULL)
            {
                TEST_LOG("NOT NULL!");
            }
            free(zero_array);
            free(non_zerro_array);
        }
    };
    struct FixturePtr: BasePtr
    {
        FixturePtr()
        {
            BasePtr();
            ptr = malloc(length);
            if(ptr == NULL)
            {
                TEST_LOG("malloc error");
                exit(1);
            }
            memset(ptr,0x55,length);
        }
    };
    struct FixturePtrNull : BasePtr
    {
        FixturePtrNull()
        {
            BasePtr();
            ptr=NULL;
        }
    };
    TEST_FIXTURE(FixturePtrNull, evoMallocSuccesTest)
    {
        CHECK_EQUAL(MEM_SUCCES, evoMalloc(&ptr, length));
        CHECK(ptr != NULL);
        CHECK_EQUAL(0, memcmp(zero_array, ptr, length));
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtrNull, evoMallocWrongLengthFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoMalloc(&ptr, 0));
        CHECK(ptr == NULL);
    }
    TEST(evoMallocWrongPtrTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoMalloc(NULL, 12));
    }
    TEST_FIXTURE(FixturePtr, eveMallocPtrNotNullFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR,evoMalloc(&ptr, length));
        CHECK(ptr != NULL);
        memset(ptr,0xff,length);
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtrNull, evoCallocSuccesTest)
    {
        CHECK_EQUAL(MEM_SUCCES, evoCalloc(&ptr, length, length));
        free(zero_array);
        zero_array = calloc(length,length);
        CHECK(zero_array != NULL);
        CHECK(ptr != NULL);
        CHECK_EQUAL(0, memcmp(zero_array, ptr, length));
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtrNull, evoCallocWrongNumFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoCalloc(&ptr, 0, length));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtrNull, evoCallocWrongLengthFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoCalloc(&ptr, length, 0));
        CHECK(ptr == NULL);
    }
    TEST(evoCallocWrongPtrFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoCalloc(NULL, 12, 12));
    }
    TEST_FIXTURE(FixturePtr, evoCallocNotNullPtrFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR,evoCalloc(&ptr, length, length));
        CHECK(ptr != NULL);
        memset(ptr, 0xff, length*length);
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtr, evoReallocSuccessUpSuccesTest)
    {
        CHECK_EQUAL(MEM_SUCCES, evoRealloc(&ptr,length*2));
        CHECK(ptr != NULL);
        memset(&((char*)ptr)[length], 0x55,length);
        CHECK_EQUAL(0, memcmp(non_zerro_array, ptr, 2*length));
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtr, evoReallocSuccessDownSuccesTest)
    {
        CHECK_EQUAL(MEM_SUCCES, evoRealloc(&ptr, length / 2));
        CHECK(ptr != NULL);
        CHECK_EQUAL(0, memcmp(non_zerro_array, ptr, length / 2));
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtr, evoReallocWrongLengthFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoRealloc(&ptr, 0));
        CHECK(ptr == NULL);
    }
    TEST(evoReallocWrongNullFailureTest)
    {
        CHECK_EQUAL(MEM_ERROR, evoRealloc(NULL, 12));
    }
    TEST_FIXTURE(FixturePtrNull, evoReallocNullPtrFailureest)
    {
        CHECK_EQUAL(MEM_ERROR,evoRealloc(&ptr, length*2));
        CHECK(ptr == NULL);
    }

    TEST(evoFreeTestSucces)
    {
        void* ptr=calloc(1,12);
        if(ptr == NULL)
        {
            printf("malloc error");
            exit(1);
        }
        CHECK_EQUAL(MEM_SUCCES, evoFree(&ptr));
        CHECK(ptr == NULL);
    }
    TEST_FIXTURE(FixturePtrNull, evoFreeTest2)
    {
        CHECK_EQUAL(MEM_ERROR,evoFree(&ptr));
        CHECK(ptr == NULL);
    }
}
}

