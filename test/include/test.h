/// \file
/// \brief      Main test
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

#define TEST_LOG(MESSAGE) fprintf(stderr,"%s:%i: warning: %s: %s", __FILE__, __LINE__, __FUNCTION__, MESSAGE "\n");

#include <string.h>
#include "UnitTest++.h"
#include "logger.h"

#endif //TEST_H_INCLUDED
