#file
#brief      after test build script
#author     Alexander Kalyuzhnyy
#date       2016
#Copyright© GPL

valgrind  --leak-check=full --track-origins=yes --leak-check=full --show-leak-kinds=all bin/test/default 2>&1|\
sed 's/==[0-9]\+==/====/'  | tee test.log
echo -n "Run tests at `date --rfc-3339=ns`" >> test.log
