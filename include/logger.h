/// \file
/// \brief      Logger
/// \author     Alexander Kalyuzhnyy
/// \date       2015-2016
/// \Copyright© GPL

#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#include <stdio.h>
extern FILE* logFile;

#define DEBUG 4
#define INFO 3
#define NOTICE 2
#define WARNING 1
#define ERROR 0

#ifndef LOG_DBG_LEVEL
#define LOG_DBG_LEVEL DEBUG
#endif //LOG_DBG_LEVEL

void write_log(
    const char* file,
    unsigned int line,
    const char* function,
    const char* dbgLvl,
    const char* format,
    ...
);

#define LOG_D(...) __LOG(DEBUG,  ## __VA_ARGS__)
#define LOG_I(...) __LOG(INFO,  ## __VA_ARGS__)
#define LOG_N(...) __LOG(NOTICE,  ## __VA_ARGS__)
#define LOG_W(...) __LOG(WARNING, ## __VA_ARGS__)
#define LOG_E(...) __LOG(ERROR,  ## __VA_ARGS__)
#ifndef __LOG
#define __LOG(DBG_LVL, ...) \
        while((unsigned int)DBG_LVL<=(unsigned int)LOG_DBG_LEVEL){ \
          write_log(__FILE__, __LINE__, __FUNCTION__, # DBG_LVL, ## __VA_ARGS__);\
          break; \
        }
#endif //__LOG
#endif // LOGGER_HPP_INCLUDED
