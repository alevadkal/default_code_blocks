/// \file
/// \brief      Defence programmind macros
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#ifndef DEFENCE_H_INCLUDED
#define DEFENCE_H_INCLUDED
#ifdef FULL_UNSAFE
#define IF(a) a;
#define ASSERT(a) while(0)

#endif // FULL_UNSAFE

#ifndef IF
#define IF(a) if(a)
#else
#warning IF defined by user
#endif // IF
#ifndef ASSERT
#define ASSERT(a) if(!(a))
#else
#warning ASSERT defined by user
#endif // ASSERT

#endif // DEFENCE_H_INCLUDED
