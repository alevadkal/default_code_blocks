/// \file
/// \brief      Memory wrapper
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#ifndef MEMORY_WRAPPER_H_INCLUDED
#define MEMORY_WRAPPER_H_INCLUDED
#include <stdlib.h>

///Use to return status from functions that allocate or free memory.
typedef enum
{
    MEM_SUCCES = 0,
    MEM_ERROR = 1
} tMemoryStatus;

tMemoryStatus evoMalloc(void** ptr, size_t size);
tMemoryStatus evoCalloc(void** ptr, size_t num, size_t size);
tMemoryStatus evoRealloc(void** ptr, size_t size);
tMemoryStatus evoFree(void** ptr);
#endif // MEMORY_WRAPPER_H_INCLUDED
