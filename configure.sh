# \file
# \brief      pre test build script
# \author     Alexander Kalyuzhnyy
# \date       2016
# \Copyright© GPL

git submodule init
git submodule update
cd unittest-cpp
git checkout v1.6.0
cd ./builds
cmake -G "CodeBlocks - Unix Makefiles" ../
cmake --build ./