/// \file
/// \brief      Logger
/// \author     Alexander Kalyuzhnyy
/// \date       2015-2016
/// \Copyright© GPL

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include "logger.h"
FILE* logFile = NULL;
static const char* fileName = "log.log";

static void Init(void) __attribute__((constructor));
static void DeInit(void) __attribute__((destructor));

void Init()
{
    logFile = fopen(fileName,"a");
    if(logFile == NULL)
    {
        fprintf(stderr, "Can't open %s file!", fileName);
        exit(1);
    }

}
void DeInit(void)
{
    if(fclose(logFile))
    {
        fprintf(stderr, "fclose: %i", errno);
    }
    logFile = NULL;
}
void write_log(
    const char* file,
    unsigned int line,
    const char* function,
    const char* dbgLvl,
    const char* format,
    ...
)
{
    va_list args;
    va_start(args, format);
    fprintf(logFile, "%s: %u: %s: %s: ", file, line, function, dbgLvl);

    fprintf(logFile, format, args);
    fprintf(logFile,"\n");
    va_end(args);
}
