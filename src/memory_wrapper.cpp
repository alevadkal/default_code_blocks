/// \file
/// \brief      Memory wrapper
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#include "logger.h"
#include "memory.h"
#include "memory_wrapper.h"
#include "defence.h"
tMemoryStatus evoMalloc(void** ptr, size_t size)
{
    tMemoryStatus status = MEM_SUCCES;
    ASSERT(ptr != NULL)
    {
        LOG_E("ptr is NULL");
        return MEM_ERROR;
    }
    ASSERT(*ptr == NULL)
    {
        LOG_E("*ptr not NULL");
        free(*ptr);
        *ptr=NULL;
        status = MEM_ERROR;
    }
    ASSERT(size != 0)
    {
        LOG_E("size is 0");
        return MEM_ERROR;
    }
    *ptr=calloc(1, size);
    ASSERT(*ptr != NULL)
    {
        LOG_E("calloc");
        return MEM_ERROR;
    }
    return status;
}

tMemoryStatus evoCalloc(void** ptr, size_t num, size_t size)
{
    tMemoryStatus status = MEM_SUCCES;
    ASSERT(ptr != NULL)
    {
        LOG_E("ptr is NULL");
        return MEM_ERROR;
    }
    ASSERT(*ptr == NULL)
    {
        LOG_E("*ptr not NULL");
        free(*ptr);
        *ptr=NULL;
        status = MEM_ERROR;
    }
    ASSERT(num != 0)
    {
        LOG_E("num is 0");
        return MEM_ERROR;
    }
    ASSERT(size != 0)
    {
        LOG_E("size is 0");
        return MEM_ERROR;
    }
    *ptr=calloc(num,size);
    ASSERT(*ptr != NULL)
    {
        LOG_E("calloc");
        return MEM_ERROR;
    }
    return status;
}

tMemoryStatus evoRealloc(void** ptr, size_t size)
{
    ASSERT(ptr != NULL)
    {
        LOG_E("ptr is NULL");
        return MEM_ERROR;
    }
    ASSERT(*ptr != NULL)
    {
        LOG_E("*ptr is NULL");
        return MEM_ERROR;
    }
    ASSERT(size != 0)
    {
        LOG_E("size is 0");
        free(*ptr);
        *ptr=NULL;
        return MEM_ERROR;
    }
    void* new_ptr = realloc(*ptr, size);
    ASSERT(new_ptr != NULL)
    {
        LOG_E("realoc");
        free(*ptr);
        ptr = NULL;
        return MEM_ERROR;
    }
    *ptr = new_ptr;
    return MEM_SUCCES;
}

tMemoryStatus evoFree(void** ptr)
{
    tMemoryStatus status = MEM_SUCCES;
    ASSERT(ptr != NULL)
    {
        LOG_E("ptr is NULL");
        return MEM_ERROR;
    }
    free(*ptr);
    ASSERT(*ptr != NULL)
    {

        LOG_W("*ptr is NULL");
        status = MEM_ERROR;
    }
    *ptr = NULL;
    return status;
}
