/// \file
/// \brief      Main
/// \author     Alexander Kalyuzhnyy
/// \date       2016
/// \Copyright© GPL

#include <stdio.h>
int main()
{
    printf("%s", "Hello world!");
    return 0;
}
